package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.lang.Math;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

   
 
public class EDPTG14CrearTorneoPremium extends TestBaseTG {
	
	final WebDriver driver;
	public EDPTG14CrearTorneoPremium(WebDriver driver){
		this.driver = driver; 
		PageFactory.initElements(driver, this);
	} 	
	  
	    
	/* 
	 ******PASAR A BASEPAGE  
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "tournament_name")
	private WebElement nombreTorneo;
	
	@FindBy(how = How.ID,using = "tournament_slug")
	private WebElement alias;
	
	@FindBy(how = How.ID,using = "tournament_start_date")
	private WebElement fechaComienzo;
	
	@FindBy(how = How.ID,using = "tournament_end_date")
	private WebElement fechaFin;
	
	@FindBy(how = How.ID,using = "tournament_max_teams")
	private WebElement cantidadMaximaEquipos;
	
	@FindBy(how = How.ID,using = "registration_start_date")
	private WebElement fechaComienzoInscripciones;
	
	@FindBy(how = How.ID,using = "registration_end_date")
	private WebElement fechaFinInscripciones;
	
	@FindBy(how = How.ID,using = "calendar_announcement_date")
	private WebElement fechaAnuncioCalendario;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'form-addition\']/div[2]/div[3]/div[9]/textarea")
	private WebElement requierements;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'form-addition\']/div[2]/div[3]/div[10]/textarea")
	private WebElement reglas;
	
	@FindBy(how = How.ID,using = "multiplier")
	private WebElement multiplicador;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'form-addition\']/div[2]/div[3]/div[13]/textarea")
	private WebElement premios;
	
	@FindBy(how = How.ID,using = "region_id")
	private WebElement selectRegion;
	
	//*****************************

	
	public void LogInTodosGamers(String passAmbiente,String chooseYourUser) {
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test EDPTG14 - Crear Torneo Premium");
		
		cargando(500);
		espera(500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000);
		driver.switchTo();
			nombreUsuario.sendKeys(chooseYourUser);
			contraseñaUsuario.sendKeys(passAmbiente);		
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.login.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	 
		
		//cierra el popover de vincula juegos
		
		
		/*WebElement vinculaJuegos = driver.findElement(By.cssSelector(".popover.fade.left.in"));
		vinculaJuegos.click();*/
		
 
	}
	
	public void CrearTorneo1(String apuntaA) {
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test EDPTG14 - Crear Torneo Premium");
		cargando(500);
		
		driver.get(apuntaA + "todosgamers.com/cms/tournaments/list");
		cargando(500);
		espera(500);
		
		//WebElement titulo = driver.findElement(By.cssSelector(".col-xs-12.text-center"));
		//titulo.getText();
		
		//String title = titulo.getText();
		//System.out.println(title); 
		
	}
	
	public void CrearTorneo2() { 
		//agregar ID al btnAgregar
		//driver.get("http://qa.todosgamers.com/cms/tournaments/add");
		cargando(500);
		WebElement btnAgregar = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.pull-right"));
		btnAgregar.click();
		
		
		nombreTorneo.sendKeys("qaautomation" + Math.random());
		//nombreTorneo.sendKeys("Torneo Premium Mario Bros");
		alias.sendKeys("OpraTeam" + Math.random());
		fechaComienzo.sendKeys("09122019"); 
		fechaFin.sendKeys("11122019");
		
		fechaComienzoInscripciones.sendKeys("01122019");
		fechaFinInscripciones.sendKeys("08122019");
		fechaAnuncioCalendario.sendKeys("07122019");
		 
		Select tipoDeTorneo = new Select(driver.findElement(By.id("tournament_type")));
		espera(500);
		tipoDeTorneo.selectByVisibleText("premium");
		espera(500);
		
		cantidadMaximaEquipos.sendKeys("10"); 
		 
		Select seleccionJuego = new Select(driver.findElement(By.id("game_id")));
		seleccionJuego.selectByVisibleText("counterstrike_go");
		
		Select seleccionRegion = new Select(driver.findElement(By.id("region_id")));
		espera(800);
		seleccionRegion.selectByVisibleText("na");
		
		Select seleccionRegion2 = new Select(driver.findElement(By.id("region_id")));
		espera(800);
		seleccionRegion2.selectByVisibleText("sa");
		
		espera(1000); 
		
		Select seleccionPais = new Select(driver.findElement(By.id("country_id")));
		espera(500);
		seleccionPais.selectByVisibleText("Argentina");
		espera(500); 
		
		requierements.sendKeys("None");
		reglas.sendKeys("vale todo");
		
		Select seleccionPlataforma = new Select(driver.findElement(By.id("platform")));
		espera(500);
		seleccionPlataforma.selectByVisibleText("pc");
		espera(500); 
		
		premios.sendKeys("Golden ticket willy wonka");		
		multiplicador.sendKeys("1");
		
		WebElement btnCrear = driver.findElement(By.cssSelector(".btn.btn-primary.btn-lg.right"));
		btnCrear.click();
		cargando(500);
		espera(1000);
		cargando(500);
		
/*		WebElement creadoCorrectamente = driver.findElement(By.cssSelector(".alert.alert-success.alert-dismissible"));
		String paso = creadoCorrectamente.getText();
		System.out.println();
		System.out.println(paso);
		System.out.println();
		espera(500); */
		
		System.out.println("Resultado Esperado:Debera visualizarse el torneo en la seccion torneos.");
		System.out.println();
		
		System.out.println("Fin de Test EDPTG14 - Crear Torneo Premium");
	}
	
	public void LogOutTodosGamers(String apuntaA) {	
		cargando(500); 
		espera(500);
			WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
		cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}