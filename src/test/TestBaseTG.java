package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class TestBaseTG extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	
	
	
	String chooseYourUser ="it@opratel.com"; //Usuario Premium	
	//String chooseYourUser ="comprovidanueva@gmail.com"; //Usuario No Premium
	
	
	//String passAmbiente ="123456"; //Contraseña QA it@opratel.com 
	String passAmbiente ="t0g4.0pr4"; //Contraseña PRODUCCION
	//String passAmbiente ="tg*0pr4t31"; //Contraseña DV4
	
	
	
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="https://qa."; //AMBIENTE QA
	
	//String apuntaA ="https://dev4."; //AMBIENTE DEV7
	
	String apuntaA ="https://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 
	@Test (priority=9)
	public void CrearTorneoFree() {
	EDPTG13CrearTorneoFree TP10 = new EDPTG13CrearTorneoFree(driver);
		TP10.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP10.CrearTorneo1(apuntaA);
		TP10.CrearTorneo2();
		TP10.LogOutTodosGamers(apuntaA);
	} 
	  
	
	@Test (priority=10)
	public void CrearTorneoPremium() { 
		EDPTG14CrearTorneoPremium TP11 = new EDPTG14CrearTorneoPremium(driver);
		TP11.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP11.CrearTorneo1(apuntaA);
		TP11.CrearTorneo2();
		TP11.LogOutTodosGamers(apuntaA);
	} 
	
	
	@Test (priority=11)
	public void consultaTorneo() {
	EDPTG17ConsultaTorneo TP12 = new EDPTG17ConsultaTorneo(driver);
		TP12.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP12.Torneos(apuntaA);
		TP12.consulta();
		TP12.LogOutTodosGamers(apuntaA);
	}
	
	

	@Test (priority=12)
	public void editarTorneo() {
	EDPTG18EditarTorneo TP13 = new EDPTG18EditarTorneo(driver);
		TP13.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP13.Torneos(apuntaA);
		TP13.Editar();
		TP13.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=13)
	public void activarTorneo() {
	EDPTG19ActivarTorneo TP14 = new EDPTG19ActivarTorneo(driver);
	TP14.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP14.Torneos(apuntaA);
		TP14.activar();
		TP14.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=14)
	public void desactivarTorneo() {
	EDPTG20DesactivarTorneo TP15 = new EDPTG20DesactivarTorneo(driver);
	TP15.LogInTodosGamers(passAmbiente,chooseYourUser);
	TP15.Torneos(apuntaA);
		TP15.desactivar();
		TP15.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=16)
	public void EliminarTorneo() {
		EDPTG22EliminarTorneo TP17 = new EDPTG22EliminarTorneo(driver);
		TP17.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP17.Torneos(apuntaA);
		TP17.eliminar();
		TP17.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=72)
	public void CMSTorneo() {
	EDPTG89CMSTorneos TP75 = new EDPTG89CMSTorneos(driver);
		TP75.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP75.CMSTorneo(apuntaA);
			TP75.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=73)
	public void CMSCreaTorneo() {
		EDPTG90CMSTorneosCrear TP76 = new EDPTG90CMSTorneosCrear(driver);
			TP76.LogInTodosGamers(passAmbiente,chooseYourUser);
				TP76.CrearTorneo1(apuntaA);			
				TP76.CrearTorneo2();
				TP76.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=74)
	public void CMSEditaTorneo() {
	EDPTG91CMSTorneosEditar TP77 = new EDPTG91CMSTorneosEditar(driver);
		TP77.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP77.Torneos(apuntaA);
			TP77.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=75)
	public void CMSEliminaTorneo() {
	EDPTG92CMSTorneosEliminar TP78 = new EDPTG92CMSTorneosEliminar(driver);
		TP78.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP78.Torneos(apuntaA);
			TP78.eliminar();
			TP78.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=76)
	public void CMSActivarTorneo() {
	EDPTG93CMSTorneosActivar TP79 = new EDPTG93CMSTorneosActivar(driver);
			TP79.LogInTodosGamers(passAmbiente,chooseYourUser);
				TP79.Torneos(apuntaA);
				TP79.activar();
				TP79.LogOutTodosGamers(apuntaA);
		}
	
	@Test (priority=77)
	public void CMSDesactivarTorneo() {
	EDPTG94CMSTorneosDesactivar TP80 = new EDPTG94CMSTorneosDesactivar(driver);
	TP80.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP80.Torneos(apuntaA);
			TP80.desactivar();
			TP80.LogOutTodosGamers(apuntaA);
	}
	 
 	
	
	
	
	
	
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */
